const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const GanaderoSchema = new Schema({

  nombre: { type: String, unique: true, required: true },
  correo: { type: String, unique: true },
  telefono: { type: Number, required: true },


});

mongoose.model('Ganadero', GanaderoSchema);