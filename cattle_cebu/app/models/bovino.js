const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Pesokilo = require('./pesokilo')
const Lote = require('./lote')


const BovinoSchema = new Schema({
  fechaNacimiento: { type: String, unique: true, required: true },
  descripcion: { type: String, unique: true, required: true },
  sexo: { type: String, unique: true, required: true },
  raza: { type: String, unique: true, required: true },
  lote: { type: Schema.Types.ObjectId, ref: Lote, required: true },
  pesokilo: { type: Schema.Types.ObjectId, ref: Pesokilo, required: true }
});

mongoose.model('Bovino', BovinoSchema);