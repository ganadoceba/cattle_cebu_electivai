const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PesokiloSchema = new Schema({
  kilogramos: { type: Number, required: true }
});

mongoose.model('Pesokilo', PesokiloSchema);