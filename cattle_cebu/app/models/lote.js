const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Ganadero = require('./ganadero')


const LoteSchema = new Schema({
  nombre: { type: String, unique: true, required: true },
  ubicacion: { type: String, unique: true, required: true },
  dimension: { type: String, unique: true, required: true },
  ganadero: { type: Schema.Types.ObjectId, ref: Ganadero },


});

mongoose.model('Lote', LoteSchema);