const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Bovino = mongoose.model('Bovino');
const Lote = mongoose.model('Lote');
const Pesokilo = mongoose.model('Pesokilo');

const auth = require('../middlewares/auth')

module.exports = (app) => {
  app.use('/', router);
};


router.post('/bovino', auth, (req, res, next) => {
  let bovino = new Bovino()
  bovino.fechaNacimiento = req.body.fechaNacimiento
  bovino.descripcion = req.body.descripcion
  bovino.sexo = req.body.sexo
  bovino.raza = req.body.raza
  bovino.lote = req.body.lote
  bovino.pesokilo = req.body.pesokilo
  bovino.save((err, bovinoStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })
    return res.status(200).send({ bovino: bovinoStored })
  })
});


router.get('/bovinos',auth,(req, res, next) => {
  Bovino.find((err, bovino) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!bovino) return res.status(404).send({ message: 'No existen bovino' })

    Lote.populate(bovino, { path: "lote" },
      function (err, lote) {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` })

        Pesokilo.populate(bovino, { path: "pesokilo" },
          function (err, pesokilo) {
            if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` })
            return res.status(200).send({ bovino })
          })

      })

  });
});


router.get('/bovino/:bovinoId', auth, (req, res, next) => {
  let bovinoId = req.params.bovinoId
  Bovino.findById(bovinoId, (err, bovino) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!bovino) return res.status(404).send({ message: ` no existe bovino` })
    Lote.populate(bovino, { path: "lote" },
      function (err, lote) {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` })

        Pesokilo.populate(bovino, { path: "pesokilo" },
          function (err, pesokilo) {
            if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` })
            return res.status(200).send({ bovino })
          })

          

      })

  });
});


router.put('/bovino/:bovinoId', auth, (req, res, next) => {
  let bovinoId = req.params.bovinoId

  let bovinoUpdate = req.body

  Bovino.findByIdAndUpdate(bovinoId, bovinoUpdate, (err, bovinoStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })

    return res.status(200).send({ bovino: bovinoStored })
  })
});



router.delete('/bovino/:bovinoId', auth, (req, res, next) => {
  let bovinoId = req.params.bovinoId
  Bovino.findByIdAndRemove(bovinoId, (err, bovino) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!bovino) return res.status(404).send({ message: `El bovino no existe ` })

    return res.status(200).send({ bovino })
  })
});;

