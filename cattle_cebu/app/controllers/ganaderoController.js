const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Ganadero = mongoose.model('Ganadero');
const Lote = mongoose.model('Lote');
const Bovino = mongoose.model('Bovino')
const auth = require('../middlewares/auth')


module.exports = (app) => {
  app.use('/', router);
};

router.post('/ganadero', auth, (req, res, next) => {
  let ganadero = new Ganadero()
  ganadero.nombre = req.body.nombre
  ganadero.telefono = req.body.telefono
  ganadero.save((err, ganaderoStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })
    return res.status(200).send({ ganadero: ganaderoStored })
  })
});

router.get('/ganaderos',auth,(req, res, next) => {
  Ganadero.find((err, ganadero) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petici�n: ' + err
    })
    if (!ganadero) return res.status(404).send({ message: 'No existen ganadero' })
    return res.status(200).send({ ganadero })
  });
});

router.get('/ganadero/:ganaderoId', auth, (req, res, next) => {
  let ganaderoId = req.params.ganaderoId
  Ganadero.findById(ganaderoId, (err, ganadero) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petici�n: ' + err
    })
    if (!ganadero) return res.status(404).send({ message: `no existe ganadero` })

    return res.status(200).send({ ganadero })
  })
});

router.put('/ganadero/:ganaderoId', auth, (req, res, next) => {
  let ganaderoId = req.params.ganaderoId

  let ganaderoUpdate = req.body

  Ganadero.findByIdAndUpdate(ganaderoId, ganaderoUpdate, (err, ganaderoStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })

    return res.status(200).send({ ganadero: ganaderoStored })
  })
});


router.delete('/ganadero/:ganaderoId', auth, (req, res, next) => {
  let ganaderoId = req.params.ganaderoId
  Ganadero.findByIdAndRemove(ganaderoId, (err, ganadero) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petici�n: ' + err
    })
    if (!ganadero) return res.status(404).send({ message: `El ganadero no existe ` })
    return res.status(200).send({ ganadero })
  })
});;

