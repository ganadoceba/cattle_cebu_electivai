const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Pesokilo = mongoose.model('Pesokilo');
const auth = require('../middlewares/auth')


module.exports = (app) => {
  app.use('/', router);
};

router.post('/pesokilo', auth, (req, res, next) => {
  let pesokilo = new Pesokilo()
  pesokilo.kilogramos = req.body.kilogramos
  pesokilo.save((err, pesokiloStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })
    return res.status(200).send({ pesokilo: pesokiloStored })
  })
});

router.get('/pesokilos', auth, (req, res, next) => {
  Pesokilo.find((err, pesokilo) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!pesokilo) return res.status(404).send({ message: 'No existen pesokilo' })
    return res.status(200).send({ pesokilo })
  });
});

router.get('/pesokilo/:pesokiloId', auth, (req, res, next) => {
  let pesokiloId = req.params.pesokiloId
  Pesokilo.findById(pesokiloId, (err, pesokilo) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!pesokilo) return res.status(404).send({ message: `no existe pesokilo` })

    return res.status(200).send({ pesokilo })
  })
});

router.put('/pesokilo/:pesokiloId', auth, (req, res, next) => {
  let pesokiloId = req.params.pesokiloId

  let pesokiloUpdate = req.body

  Pesokilo.findByIdAndUpdate(pesokiloId, pesokiloUpdate, (err, pesokiloStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })

    return res.status(200).send({ pesokilo: pesokiloStored })
  })
});



router.delete('/pesokilo/:pesokiloId', auth, (req, res, next) => {
  let pesokiloId = req.params.pesokiloId
  Pesokilo.findByIdAndRemove(pesokiloId, (err, pesokilo) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!pesokilo) return res.status(404).send({ message: `El pesokilo no existe ` })

    return res.status(200).send({ pesokilo })
  })
});;

