const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Lote = mongoose.model('Lote');
const Ganadero = mongoose.model('Ganadero');
const auth = require('../middlewares/auth')

module.exports = (app) => {
  app.use('/', router);
};

router.post('/lote', auth, (req, res, next) => {
  let lote = new Lote()
  lote.nombre = req.body.nombre
  lote.ubicacion = req.body.ubicacion
  lote.dimension = req.body.dimension
  lote.ganadero = req.body.ganadero
  lote.save((err, loteStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })
    return res.status(200).send({ lote: loteStored })
  })
});


router.get('/lotes', auth, (req, res, next) => {
  Lote.find((err, lote) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!lote) return res.status(404).send({ message: 'No existen lote' })

    Ganadero.populate(lote, { path: "ganadero" },
      function (err, ganadero) {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` })
        return res.status(200).send({ lote })

      })

  })

});


router.get('/lote/:loteId', auth, (req, res, next) => {
  let loteId = req.params.loteId
  Lote.findById(loteId, (err, lote) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!lote) return res.status(404).send({ message: `no existe lote` })

    return res.status(200).send({ lote })
  })
});


router.put('/lote/:loteId', auth, (req, res, next) => {
  let loteId = req.params.loteId

  let loteUpdate = req.body

  Lote.findByIdAndUpdate(loteId, loteUpdate, (err, loteStored) => {
    if (err) return res.status(500).send({
      message:
        `Error al salvar en la base de datos: ${err} `
    })

    return res.status(200).send({ lote: loteStored })
  })
});


router.delete('/lote/:loteId', auth, (req, res, next) => {
  let loteId = req.params.loteId
  Lote.findByIdAndRemove(loteId, (err, lote) => {
    if (err) return res.status(500).send({
      message:
        'Error al realizar la petición: ' + err
    })
    if (!lote) return res.status(404).send({ message: `El lote no existe ` })

    return res.status(200).send({ lote })
  })
});;

