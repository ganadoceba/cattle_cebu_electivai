const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'won-cebu'
    },
    SECRET_TOKEN:'cattlecebu123456789',
    port: process.env.PORT || 3000,
    db: 'mongodb://tiven:Tiven125@cluster0-shard-00-00-ou2hl.mongodb.net:27017,cluster0-shard-00-01-ou2hl.mongodb.net:27017,cluster0-shard-00-02-ou2hl.mongodb.net:27017/won_cebu?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
  },

  test: {
    root: rootPath,
    app: {
      name: 'won-cebu'
    },
    SECRET_TOKEN:'cattlecebu123456789',
    port: process.env.PORT || 3000,
    db: 'mongodb://tiven:Tiven125@cluster0-shard-00-00-ou2hl.mongodb.net:27017,cluster0-shard-00-01-ou2hl.mongodb.net:27017,cluster0-shard-00-02-ou2hl.mongodb.net:27017/won_cebu?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
  },

  production: {
    root: rootPath,
    app: {
      name: 'won-cebu'
    },
    SECRET_TOKEN:'cattlecebu123456789',
    port: process.env.PORT || 3000,
    db: 'mongodb://tiven:Tiven125@cluster0-shard-00-00-ou2hl.mongodb.net:27017,cluster0-shard-00-01-ou2hl.mongodb.net:27017,cluster0-shard-00-02-ou2hl.mongodb.net:27017/won_cebu?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
  }
};

module.exports = config[env];
